#################################################
#                                               #
# Author 1: Adam Alvarado Bertomeu              #
# Author 2: Bryan porras Blanch                 #
# Date: 20/03/2019                              #
# Version: 1.0                                  #
# Description: Module which will allow a client #
# to access the IBM COS account.                #
#                                               #
#################################################

import ibm_boto3
import ibm_botocore

class COSBackend(object):
    def __init__ (self, config): 
        client_config = ibm_botocore.client.Config(max_pool_connections=200)
        access_key = config["access_key"]
        endpoint = config["endpoint"]
        secret_key = config["secret_key"]

        # create client object
        self.cos_client = ibm_boto3.client("s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key, config=client_config, endpoint_url=endpoint)

    def put_object(self, bucket_name, key, data):
        """
        Put an object in COS. Override the object if the key already exists.
        :param key: key of the object.
        :param data: data of the object
        :type data: str/bytes
        :return: None
        """
        try:   
            res = self.cos_client.put_object(Bucket=bucket_name, Key=key, Body=data)
            status = 'OK' if res['ResponseMetadata']['HTTPStatusCode'] == 200 else 'Error'
            
        except ibm_botocore.exceptions.ClientError as e:
                return -1

    def get_object(self, bucket_name, key, stream=False, extra_get_args={}):
        """
        Get object from COS with a key. Throws StorageNoSuchKeyError if the given key does not exist.
        :param key: key of the object
        :return: Data of the object
        :rtype: str/bytes
        """
        try:
            r = self.cos_client.get_object(Bucket=bucket_name, Key=key, **extra_get_args)
            if stream:
                data = r['Body']
            else:
                data = r['Body'].read()
            return data
        except ibm_botocore.exceptions.ClientError as e:
                return -1
    
    def head_object(self, bucket_name, key):
        """
        Head object from COS with a key. Throws StorageNoSuchKeyError if the given key does not exist.
        :param key: key of the object
        :return: Data of the object
        :rtype: str/bytes
        """
        try:
            metadata = self.cos_client.head_object(Bucket=bucket_name, Key=key)       
            return metadata['ResponseMetadata']['HTTPHeaders']['content-length']
        except ibm_botocore.exceptions.ClientError as e:
                return -1
    
    def delete_object(self, bucket_name, key):
        """
        Delete an object from storage.
        :param bucket: bucket name
        :param key: data key
        """
        return self.cos_client.delete_object(Bucket=bucket_name, Key=key)
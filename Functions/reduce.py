from Libs import cos_backend
import json
import re

def main(args):
    res = args['res']
    partitions = args['partitions']
    cos = cos_backend.COSBackend(res['ibm_cos'])

    result = {}
    i=0
    
    while(i < int(partitions)):
        map_partition = json.loads(cos.get_object(args['bucket'], args['file_name'] + str(i) + '.map').decode('latin-1'))
        cos.delete_object(args['bucket'], args['file_name'] + str(i) + '.map')
        
        for item in map_partition.keys():
            if item in result:
                result[item]=map_partition[item]+result[item]
            else:
                result[item]=map_partition[item]

        i+=1
    
    cos.put_object(args['bucket'], args['file_name'] + '.reduce', json.dumps(result))
    
    return {"status":"ok"}
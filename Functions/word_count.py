from Libs import cos_backend
import json
import re

def main(args):
    res = args['res']
    cos = cos_backend.COSBackend(res['ibm_cos'])
    range_bytes = 'bytes={}-{}'.format(args['initial'], args['final'])
    part=cos.get_object(args['bucket'], args['file_name'], extra_get_args={'Range': range_bytes}).decode('latin-1').lower()
    part = re.sub(r'[-,;.:?¿!¡\'\(\)\[\]\"*+-_<>#$€&^%|]', " ", part).split()

    result = {}

    for word in part:
        if word in result:
            result[word] = result[word] + 1
        else:
            result[word] = 1

    cos.put_object(args['bucket'], args['file_name'] + str(args['partition']) + '.map', json.dumps(result))

    return {"status":"ok"}
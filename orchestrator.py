from Libs import cos_backend
from Libs import ibm_cf_connector
import yaml
import sys
import time

def executecf(cf, function, range_size, module, file_name, partitions, res, params):

    i=0
    initial=0
    final=range_size

    while(i < partitions):
        params["initial"] = initial
        params["final"] = final
        params["partition"] = i

        cf.invoke(function, params)
                
        initial=final+1
        final+=range_size
        if((i+2)==partitions):
            final+=module
        i+=1

def executeReduce(cf, cos, file_name, params, result_file_name):
    cf.invoke('reduce', params)

    result_file=cos.get_object('adam.alvarado.sd', file_name + '.reduce')
    
    while(result_file == -1):
        result_file=cos.get_object('adam.alvarado.sd', file_name + '.reduce')

    with open(result_file_name, 'w') as result:
	    result.write(result_file.decode('unicode-escape'))

    cos.delete_object('adam.alvarado.sd', file_name + '.reduce')

def main():
    if len(sys.argv) != 3:
        print("Parameters: orchestrator.py data-file-name partitions")
        print("Example to use: python3 orchestrator.py pg2000.txt 10")
        exit(1)

    with open('ibm_cloud_config.yaml', 'r') as config_file:
        res = yaml.safe_load(config_file)

    cos=cos_backend.COSBackend(res['ibm_cos'])
    cf = ibm_cf_connector.CloudFunctions(res['ibm_cf'])

    file_name=sys.argv[1]
    file_size=int(cos.head_object(res['ibm_cos']['object_storage_name'],file_name))
    if(file_size == -1):
        exit(1)
    partitions=int(sys.argv[2])

    range_size=int(file_size/partitions)
    module=int(file_size%partitions)
    params={}
    params["res"] = res
    params["file_name"] = file_name
    params["bucket"] = 'adam.alvarado.sd'
    params["partitions"] = partitions

    #Get time counting words
    start_counting_words = time.time()
    executecf(cf, 'counting_words', range_size, module, file_name, partitions, res, params)
    executeReduce(cf, cos, file_name, params, 'counting_words_'+file_name)
    end_counting_words = time.time()

    #Get time word count
    start_word_count = time.time()
    executecf(cf, 'word_count', range_size, module, file_name, partitions, res, params)
    executeReduce(cf, cos, file_name, params, 'word_count_'+file_name)
    end_word_count = time.time()

    print("Time counting words: " + str(end_counting_words - start_counting_words))
    print("Time word count: " + str(end_word_count - start_word_count))

    exit(0)

if __name__ == "__main__":
    main()